const Course = require("../models/course")


//view all active courses
module.exports.getAllActive = () => {
	return Course.find(Course.isActive).then(resultsFromFind => resultsFromFind);
};

//create/register a course
module.exports.add = (params) => {
	let newCourse = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	});

	return newCourse.save().then((course, err) => {
		return (err) ? false : true
	});
};

//get a specific course
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(resultFromFindById => resultFromFindById);
};

//modify course details
module.exports.update = (params) => {
	let updatedCourse = {
		name: params.name,
		description: params.description,
		price: params.price
	};

	return Course.findByIdAndUpdate(params.courseId, updatedCourse).then((course, err) => {
		return (err) ? false: true
	});
};

// //add true update
// module.exports.update2 = (params) => {
// 	let updatedCourse = {
// 		isActive: params.isActive
// 	};

// 	return Course.findByIdAndUpdate(params.id, updatedCourse).then((course, err) => {
// 		return (err) ? false: true
// 	});
// };

//archive a course - set isActive to false
module.exports.archive = (params) => {
	let updateActive = {
		isActive: false
	};

	return Course.findByIdAndUpdate(params.courseId, updateActive).then((course, err) => {
		return (err) ? false : true
	});
};

//archive a course - set isActive to false
module.exports.unarchive = (params) => {
	let updateActive = {
		isActive: true
	};

	return Course.findByIdAndUpdate(params.courseId, updateActive).then((course, err) => {
		return (err) ? false : true
	});
};